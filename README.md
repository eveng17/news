# Syllabus and News for CS594/690: Evidence Engineering

[Syllabus](https://bitbucket.org/EvEng17/news/raw/master/ee.pdf)

# Final presentations: May 5, Mk434, 10:15-12:15


# Second chapter: 
* Tapajit - Factor
* Yuxing  - Additive models
* Sara    - Simulation-based inference
* Eduardo - Appendix I: Chi^2+LR
* Walton  - Regression basics

# Class on Tu Feb 21

* Eduardo finished his presentation on regression trees. The notebook and the notes are here:
https://bitbucket.org/eveng17/news/src/8418547f76f9b2bc44a6a66393e9ef3f00527952/chapter13/?at=master

* Sara presented here analysis of read and write data as well as discussed the role of simulation
 with some details. The iPython code is here: https://bitbucket.org/eveng17/news/src/7c8034b7c981b2f01a2e7c59c2484c47dadf478e/chapter5-R-code/?at=master

# Class on Tu Feb 14

We will start the class by  attending the talk  at 11AM in MK435. The talk  
has some relevance to the class and concerns variability of tasks in HPC and how engineer OS to mitigate that 
problem. After the talk we will go to our regular 
meeting room. Please not that the talk starts 10 min before the 
class stat time. Please try to join when your schedule 
allows. 

# Class on Tu Jan 31
      * Presentation of Simian army: what are the anti-fragile organizations?
	  * Presentation on security models: what are the data-driven security model after all?
	  * Analysis of linux logs in infrastructure/linux.md
	  * Examples for neo4j infrastructure/neo4j.md

# Class on Tu Jan 24 Note the room change
      * Henceforth the class will be in MK 639 on Tuesday�s from 11 until 1:40

# Class on Th, Jan 17

- Paper and chapter assignments
      * Sara: Chaos Monkey & Chapter 5
      * Joe:  Testing under Uncertainty & Chapter 8
      * Walton: DevOps & Chapter 15
      * Tapajit: Netflix Recommender & Chapter 3
      * Eduardo: ODP? & Chapter 13
      * Yuxing: GoogleSecurity & Chapter 16 

- The class will be on Tuesday only, next class Jan 24 in  MK 639 11 until 1:40

- I expect some progress on framing your project, in particular, what research questions will be addressed, how, and how that will save the world.

- I expect you to read the paper and the chapter so you can say what your impressions were, if you'd like to change, etc. 

- Please send (or set up on your own) rsa public key. 

- Please try to login [https://bitbucket.org/eveng17/infrastructure/src/master/README.md]


# Class on Th, Jan 12

- Key info:
      * Class site: https://bitbucket.org/eveng17/
      * Syllabus: https://bitbucket.org/EvEng17/news/raw/master/ee.pdf
      * Lecture slides for today: https://bitbucket.org/EvEng17/papers/raw/master/course1.pdf
- To do before next Tuesday: 
      1. Please send me your BitBucket id so I can add you to the BB organization.
      1. Please think some more about the project you'd like to work on for this class. 
      1.  Please think about which day (Tu or Th would be better for you as  a regular meeting time)
      1.  Please think of what snacks you would prefer for the extended class.
      1. Please take a look at the list of papers to present in class (in the folder "papers")
      1. Please take a look at the chapters of Advanced Data Analysis book (http://www.stat.cmu.edu/~cshalizi/ADAfaEPoV/) to express preferences about which methods you would prefer to cover.