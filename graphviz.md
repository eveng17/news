Illustrations of graphviz
=========================

Dot (or now graphviz) is a very powerful and scalable 
tool developed by Lefty (Elefterios) at AT&T Bell Labs in the 90ies.

For some examples of what it can do see:
http://www.graphviz.org/Gallery.php


As shown in the bbasic example below the graph is specifies as a set of links:
```
echo "digraph G {Hello->World}" | dot -Tpng >hello.png
```


You can also use dotty for an interactive version or produce svg/ps/other formats 
for images.


Each link (and node) can have certain properties, e.g., 

```
echo "digraph G {Hello [label="Goodby" shape=box]; Hello->World [label="Link"] }" | dot -Tpng >hello.png
```


