Cliff notes on embeddings: LSI, LDA, word2vec
==============================


Term/document matrix T tends to be very large for a large corpus and
the content search via keyword (is the entry in T_kd for keyword k
in document d nonzero?) may:

1. Miss synonyms car/auto
1. Get wrong documents (bank as in a financial institution or as in
   a river).

The dimensions of this matrix is reduced via various approaches

Latent Semantic Indexing (LSI)
------------------------

The idea is similar to PCA (principal Component Analysis), that the
multi-dimensional dataset tends to vary in a very small set of
dimensions.

The approach is to find a singular value decomposition of T, i.e., 
obtain three matrixes: T = USV^t,
where S represents the diagonal matrix of
eigenvalues (singular values as the matrix is not symmetric or full
rank), and V_t is called SVD document matrix.

By changing low singular values in S to zero, we get a low rank
(typically around k=400) matrix S_k and the transformed
term-document matrix T_k= US_kV^t has now only k rows corresponding
to the largest singular values. The V^t is also replaced by assigning zero below the k-th row to
speed the calculation to
S_kV_k^t.

The LSI is primarily used for search: a query q represented as term
vector is transformed using left singular vectors U  and inverse
singular values q_k = S_k^-1U_k^tq. Then the cosine similarity
with a document (from V) and q_k can be used to find the most
similar document.


For further reading [a more detailed elaboration of LSI](http://nlp.stanford.edu/IR-book/html/htmledition/latent-semantic-indexing-1.html),
and [a very simple example](http://www1.se.cuhk.edu.hk/~seem5680/lecture/LSI-Eg.pdf).



Latent Dirichlet Allocation (LDA)
------------------------

Unlike LSI, LDA is *a generative* model that describes how the
documents in a dataset were created.

It starts from the concept of topics, lets assume we have
k topics. Each topic t is a multinomial probability distribution
over all words. For example, "cat" topic may have high probability
for cat, catfood, feline. Lower probability for dog, and very low
probability for banking. Nevertheless, the probability, even if
zero, has to be defined for all possible words. 

A document from the point of LDA is defined by another multinomial
distribution d that defines a specific probability of each topic t_i
(i= 1..k). To generate a document we simple randomly sample a topic, then a
word from that topic for each word of the document.

In real life we do not know the number of topics, the topics
themselves or any of the described distributions. Even though LDA
is generative, it is not used to generate the documents, the
parameters are estimated from the corpus.

The intuition behind the estimation of parameters is of Bayesian
origin. The conjugate prior distribution for the multinomial
distribution is Dirichlet. Typically a uniform prior is chosen
(not sure about the scale). Given that word distributions are
also multinomial for each topic, we have a hierarchical model
with a lot of parameters: k-1 topic probabilities, and
n term probabilities for each topic: (k-1)*n: a huge number.

However, the distribution of the observed words can be written
explicitly due to the generative nature of the LDA. The number of
parameters makes the computation of posterior distribution
a big problem. As described
[here](http://obphio.us/pdfs/lda_tutorial.pdf)
the likelihood is greatly simplified in order to get any results
in practice. 


Unlike LSI, the purpose of LDA is to obtain the topic distribution.
Often each topic may appear to be meaningful based on the collection
of terms with the highest probabilities. LDA can be used for search
as well. If we want to find documents similar to a specific
document we pick topic distribution and find other documents that
have a similar topic distribution. The similarity metrics of choice
is no longer a cosine similarity, however, because we compare
probability distributions. Kullback-Leibler divergence (KL) or related
measures are used in this case.

Notably, the quality of the topics obtained by LDA was not evaluated
for being meaningful until [recently](http://cs.colorado.edu/~jbg/docs/nips2009-rtl.pdf).

Word2Vec
--------

Word2vec has absolutely no generative or other intuitive model
behind it. In fact, it is a collection of related algorithms,
not a single algorithm. The best place to see various uses is its
googlecode website of the original
[c code](https://code.google.com/archive/p/word2vec/)
and [groups](https://groups.google.com/forum/#!forum/word2vec-toolkit)
or [gensim])https://groups.google.com/forum/#!forum/gensim).


If you already have some background on the topic, it may be worth
reading a discussion trying to get the intuition for why
various embeddings [work](http://www.offconvex.org/2016/02/14/word-embeddings-2/).
In particular, the model of the narrative as a random walk. 

For more details please read [an example of word2vec](http://www-personal.umich.edu/~ronxin/pdf/w2vexp.pdf). 